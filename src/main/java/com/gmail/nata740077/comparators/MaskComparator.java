package com.gmail.nata740077.comparators;

public class MaskComparator extends ComparatorAbstract{

    public MaskComparator(String stringName) {
        super(stringName);
    }

    @Override
    public boolean compare(String fileName) {
        stringName = stringName.replaceAll("[^\\da-z]", "");
        if (fileName.endsWith(stringName)) {
            return true;
        }
        return false;
    }
}
