package com.gmail.nata740077;

import com.gmail.nata740077.comparators.*;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class ComparatorTest {
    @Test
    public void emptyComparatorTest() {
        ComparatorAbstract comparator = ComparatorBuilder.build("");
        assertTrue(comparator instanceof EmptyComparator);
    }

    @Test
    public void maskComparatorTest() {
        ComparatorAbstract comparator = ComparatorBuilder.build("*.java");

        assertTrue(comparator instanceof MaskComparator);
    }

    @Test
    public void nameComparatorTest() {
        ComparatorAbstract comparator = ComparatorBuilder.build("file-25645735.java");

        assertTrue(comparator instanceof NameComparator);
    }

    @Test
    public void regexComparatorTest() {
        ComparatorAbstract comparator = ComparatorBuilder.build(".*?[a-z]{4}-\\\\d+\\.[a-z]+");

        assertTrue(comparator instanceof RegexComparator);
    }

//    @Test(expected = RuntimeException.class)
//    public void errorComparatorTest() {
//        ComparatorAbstract comparator = ComparatorBuilder.build("file");
//    }
}