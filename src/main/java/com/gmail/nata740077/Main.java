package com.gmail.nata740077;

import com.gmail.nata740077.comparators.ComparatorBuilder;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;


public class Main {
    public static void main(String[] args) throws ParserConfigurationException, SAXException {
        String inputFileName;
        String stringName;

        if (args.length == 2 && args[0].equals("-f")) {
            inputFileName = args[1];
            stringName = "";
        } else if (args.length == 4 && args[0].equals("-f") && args[2].equalsIgnoreCase("-s")) {
            inputFileName = args[1];
            stringName = args[3];
        } else {
            System.out.println("Не верные параметры ввода");
            return;
        }

        SAXParserFactory factory = SAXParserFactory.newInstance();
        DefaultHandler handler = new Handler(ComparatorBuilder.build(stringName));


        SAXParser parser = factory.newSAXParser();
        try {
            parser.parse(new File(inputFileName), handler);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}

