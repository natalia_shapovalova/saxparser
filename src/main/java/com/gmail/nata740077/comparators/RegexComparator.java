package com.gmail.nata740077.comparators;

import java.util.regex.Pattern;

public class RegexComparator extends ComparatorAbstract{
    public RegexComparator(String stringName) {
        super(stringName);
    }

    @Override
    public boolean compare(String fileName) {
        if (Pattern.matches(stringName, fileName)) {
            return true;
        }
        return false;
    }
}
