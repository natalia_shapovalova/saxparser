package com.gmail.nata740077.comparators;

public abstract class ComparatorAbstract {
    public String stringName;

    public ComparatorAbstract (String stringName){
        this.stringName = stringName;
    }

    abstract public boolean compare(String fileName);
}
