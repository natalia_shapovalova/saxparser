package com.gmail.nata740077.comparators;

public class EmptyComparator extends ComparatorAbstract {
    public EmptyComparator(String stringName) {
        super(stringName);
    }

    @Override
    public boolean compare(String fileName) {
        return true;
    }
}
