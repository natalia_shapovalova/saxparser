package com.gmail.nata740077.comparators;

public class ComparatorBuilder {

    public static ComparatorAbstract build(String stringName) {
        if (stringName.isEmpty()) {
            return new EmptyComparator(stringName);
        } else if (stringName.matches("[a-z|0-9-._]+")) {
            return new NameComparator(stringName);
        }
        // stringName = stringName.substring(1, stringName.length() - 1); //убираем '
        if (!isRegex(stringName)) {
            return new MaskComparator(stringName);
        } else {
            return new RegexComparator(stringName);
        }

      //  throw new RuntimeException("Неизвестный запрос");

    }


    public static boolean isRegex(String str) {
        try {
            java.util.regex.Pattern.compile(str);
            return true;
        } catch (java.util.regex.PatternSyntaxException e) {
            return false;
        }
    }
}


