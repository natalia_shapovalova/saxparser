package com.gmail.nata740077;


import com.gmail.nata740077.comparators.ComparatorBuilder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class HandlerTest {

    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Test
    public void emptyFilterTest() throws ParserConfigurationException, SAXException {
        parseTestFile("");

        assertEquals("""
                //file-77194797.xml\r
                //dir-880971375/file-90738721998.java\r
                //dir-880971375/dir-219753795/file-97484702197.xhtml""", systemOutRule.getLog().trim());
    }

    private void parseTestFile(String s) throws ParserConfigurationException, SAXException {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        DefaultHandler handler = new Handler(ComparatorBuilder.build(s));

        SAXParser parser = factory.newSAXParser();
        try {
            parser.parse(new File("forTest.xml"), handler);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void maskFilterTest() throws ParserConfigurationException, SAXException {
        parseTestFile("*.java");

        assertEquals("//dir-880971375/file-90738721998.java", systemOutRule.getLog().trim());
    }

    @Test
    public void nameFilterTest() throws ParserConfigurationException, SAXException {
        parseTestFile("file-97484702197.xhtml");

        assertEquals("//dir-880971375/dir-219753795/file-97484702197.xhtml", systemOutRule.getLog().trim());
    }

    @Test
    public void regexFilterTest() throws ParserConfigurationException, SAXException {
        parseTestFile(".*?[a-z]{4}-\\d+.[a-z]+");

        assertEquals("""
                //file-77194797.xml\r
                //dir-880971375/file-90738721998.java\r
                //dir-880971375/dir-219753795/file-97484702197.xhtml""", systemOutRule.getLog().trim());
    }

}
