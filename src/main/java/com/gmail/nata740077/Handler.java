package com.gmail.nata740077;

import com.gmail.nata740077.comparators.ComparatorAbstract;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;

public class Handler extends DefaultHandler {
    ComparatorAbstract comparator;

    public Handler(ComparatorAbstract comparator) {
        this.comparator = comparator;
    }

    ArrayList<String> way = new ArrayList<>();
    Boolean isFile = false;
    String currentFileName;

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        super.startElement(uri, localName, qName, attributes);
        if (qName.equals("child") || qName.equals("node")) {
            isFile = Boolean.parseBoolean(attributes.getValue("is-file"));
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        if (qName.equals("child")) {
            if (isFile && comparator.compare(currentFileName)) {
                System.out.println(String.join("/", way) + "/" + currentFileName);
                currentFileName = "";
                isFile = false;
            } else if (!isFile){
                way.remove(way.size() - 1);
            }

        }
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        String str = new String(ch, start, length);
        if (str.trim().length() > 0 && !isFile ) {
            way.add(str);
            currentFileName = "";
        } else if (str.trim().length() > 0){
            currentFileName = str;
        }
    }
}
