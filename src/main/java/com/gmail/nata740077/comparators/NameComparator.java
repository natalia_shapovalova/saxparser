package com.gmail.nata740077.comparators;

public class NameComparator extends ComparatorAbstract {
    public NameComparator(String stringName) {
        super(stringName);
    }

    @Override
    public boolean compare(String fileName) {
        if (fileName.equals(stringName)){
            return true;
        }
        return false;
    }
}
